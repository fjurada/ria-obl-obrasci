<?php

interface StrategyInterface
{
	public function filter($user);
}

class NameStrategy implements StrategyInterface //bilokoji strategy mora implementirat ovaj interface ako će radit sa modelom
{
	public function filter($user) 	// vratiti true ako prosljeđeno ime počinje sa m
	{
		return $user[0] == 'M';
		
		/* isto kao ovo gore ^^
		if ($user[0] == M)
		{
			return True;
		}
		else return False;
		*/
	}	
}

//Vraća random usere iz polja sa 80% šanse za uspjeh
class RandomStrategy implements StrategyInterface
{
	public function filter();
		return rand(1,10) > 2;
}

class User extends Model 
{
	$all_users = array();
	
	// osigurali smo se od usera koje nisu array  i koje nemaju StrategyInterface
	public function __construct(array $users = array(), StrategyInterface $strat) 
	{
		$this->all_users = $users;
		$this->strategy = $strat;
	}
	
	//uz korištenje nekog filtera pronalazimo imena u polju
	public function find()
	{
		if ($this->strategy === null)
		{
			return $this->all_users;
		}
		
		$result = array();
		foreach ($this->all_users as $user)
		{
			if($this->strategy->filter($user))
				$result[] = $user;
		}
		return $result;
	}
	
	//Omogućuje mjenjanje strategije u
	public function set_strategy(StrategyInterface $strat)
	{
		$this->strategy = $strat;
	}
}

//korištenje

$user_array = array("Marko", "Ivan", "Josip", "Matej", "Branko");
$some_user = new User($user_array, new NameStrategy());

$filtered_users1=$some_user->find();
print_r($filtered_users1); //Svi korisnici sa početnim slovom M

$some_user->set_strategy(new RandomStrategy());

$filtered_users2=$some_user2->find();
print_r($filtered_users2); //Random korisnici, sa 80% šanse
