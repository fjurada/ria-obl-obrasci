//implementirati query builder
<?php

class Builder {
	
	private $query = "";
	private $table;
	private $columns = array(["*"]);
	public function selectFrom($table)
	{
		$this->table = $table;
		return $this; //OVO NAM DOPUŠTA da radimo chaining, znači $query= builder->selectFrom("*")->selectColumns(blabal)->build();
	}
	
	public function selectColumns(array $columns)
	{
		$this->columns = $columns
		return $this;
	}
	
	public function build()
	{
		$this->query = "SELECT ";
		foreach ($this->columns as $col)
		{
			$query .= $col . ", ";
		}
		return $query = rtrim($query, ", "). " FROM ".$table;
	}
}





$builder = new Builder();
$builder->selectFrom('user');
$builder->selectColumns(['username', 'id']);

