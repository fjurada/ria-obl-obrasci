<php?

function db() {
	return Db::Instance();
}

class Db {
	static $inst = null;
	
	public static function Instance() {
		if (self::$inst === null) {
			self::$inst = new self();
			self::$inst->mysql = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
			  'username' => 'student',
			  'password' => 'riteh',
			  'dbname' => 'student',
			));
		}
		return self::$inst;
	}
	
	pubic function query($sql) {
		return self::$inst->mysql->execute($sql);
	}
}