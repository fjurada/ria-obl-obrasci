<?php

interfaca Locker
{
	public function canRead();
	private function putLock();
	private function unlock();
}

class LockFile
{
	public function createLock($file)
	{
		if (file_exists($file)) //file_exists bi trebao dobiti puni path, zbog nekih sustava itd
		{
			file_put_contents ($file.".locked", "Locked");
		}
	}
	public function removeLock($file)
	{
		if (file_exists($file.".locked"))
		{
			unlink ($file.".locked");
		}
	}
	public function checkLock($file)
	{
		return file_exists($file.".locked");
	}
}

/*use case: ???

*/