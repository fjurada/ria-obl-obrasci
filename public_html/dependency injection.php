dependency injection - treba odvojit logiku aplikacije u smislu:
dodavanja vanjskih struktura bez diranja unutrašnjih struktura samog
identiteta.

<?php

class Book {
	
	public function __construct($bookname, $authorname)
	{
		$this->author = new Author($authorname);
		$this->bookName = $bookname;
	}
	
}

$newBook = new Book("Šegrt Hlapić", "Marko");
echo $newBook->author->name;


//-i sad ako bimo promjenili klasu Autor, onda nastaju problemi jer su objekti usko povezani. type decoupling!!
//rjesenje:


class Book {
	
	public function __construct($bookname, Author $author)
	{
		$this->author = $author;
		$this->bookName = $bookname;
	}
}

$author = new Author(array $Npr, "marko");  //nešto decoupling se taj način zove

$newBook = new Book("Šegrt Hlapić", $author);
echo $newBook->author->name;
