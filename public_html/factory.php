<php?
$type = 'admin';
$data = array('username' => 'Filip');

function secret() {
	return uniq_id();
}

class User {
	$type = "";
	$username = "";
}

class Admin {
	public function __contruct($attr) {
		$this->username = $attr['Username'];
		$this->secret_key = secret();
	}
}

class Factory {
	private static $priv = array('user' => 'User', 'admin'=>'Admin');
	
	public static function create($type, array $attr = []) {
		if (array_key_exists($type), Factory::$priv) && class_exists(Factory::$priv[$type])) {
			$tmp = new Factory::$priv[$type];
			return new $tmp($attr);
		}
	}
}
$user = Factory::create($name, $data);